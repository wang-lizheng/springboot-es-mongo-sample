package com.example.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootElasticsearchSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootElasticsearchSampleApplication.class, args);
    }

}
