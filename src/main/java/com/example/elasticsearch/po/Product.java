package com.example.elasticsearch.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * 商品实体类
 *
 * @author Liheng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(indexName = "ieecu_shopping")
public class Product {

    @Id
    private Long id;

    @Field(type = FieldType.Object, analyzer = "ik_max_word")
    private String title;

    @Field(type = FieldType.Keyword)
    private String category;

    @Field(type = FieldType.Double)
    private Double price;

    @Field(type = FieldType.Keyword, index = false)
    private String images;

}
