package com.example.elasticsearch.po;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "t_user")
public class User {

    @Id
    private Long id;

    private String name;

    private String passwd;

    private String city;

    private String address;
}
