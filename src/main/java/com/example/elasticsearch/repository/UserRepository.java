package com.example.elasticsearch.repository;

import com.example.elasticsearch.po.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository(value = "userRepository")
public interface UserRepository extends MongoRepository<User,Long> {
}
