package com.example.elasticsearch.repository;

import com.example.elasticsearch.po.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Product Repository
 *
 * @author Liheng
 */
public interface ProductRepository extends ElasticsearchRepository<Product, Long> {
}
