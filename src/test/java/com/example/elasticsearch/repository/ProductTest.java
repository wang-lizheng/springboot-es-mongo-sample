package com.example.elasticsearch.repository;

import com.example.elasticsearch.po.Product;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ProductTest {

    @Autowired
    private ProductRepository productRepository;


    @Test
    public void create() {
        Product product = new Product(100L, "小米手机", "小米", 2399.00, "http://www.baidu.com/1.jpg");
        productRepository.save(product);
    }

    @Test
    public void query() {
        Product product = productRepository.findById(1000L).get();
        System.out.println(product);
    }

    @Test
    public void update() {
        Product product = new Product(1000L, "小米新手机", "小米", 2399.00, "http://www.baidu.com/1.jpg");
        productRepository.save(product);
    }

    @Test
    public void delete() {
        productRepository.deleteById(1000L);
    }

    @Test
    public void list() {
        Iterable<Product> products = productRepository.findAll();
        for (Product product : products) {
            System.out.println(product);
        }
    }

    @Test
    public void saveAll(){
        List<Product> productList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Product product = new Product();
            product.setId(Long.valueOf(i));
            product.setTitle("["+i+"]apple");
            product.setCategory("手机");
            product.setPrice(1999.0+i);
            product.setImages("http://www.baidu.com/xm.jpg");
            productList.add(product);
        }
        productRepository.saveAll(productList);
    }

    @Test
    public void search() {
        TermQueryBuilder termQuery = QueryBuilders.termQuery("title", "小米手机");
        Iterable<Product> products = productRepository.search(termQuery);
        for (Product product : products) {
            System.out.println(product);
        }
    }

    @Test
    public void searchPages() {
        Pageable pageable = PageRequest.of(0,5);
        Page<Product> all = productRepository.findAll(pageable);
        all.getContent().forEach(System.out::println);
    }
}
