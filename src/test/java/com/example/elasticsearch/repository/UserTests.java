package com.example.elasticsearch.repository;

import com.example.elasticsearch.po.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest
@Slf4j
public class UserTests {

    @Autowired
    private UserRepository userRepository;

    //保存
    @Test
    public void addTest() {
        User user1 = new User();
        user1.setId(1L);
        user1.setName("张三");
        user1.setPasswd("123456");
        user1.setCity("北京");
        user1.setAddress("上地三街");

        User user2 = new User();
        user2.setId(2L);
        user2.setName("李四");
        user2.setPasswd("123456");
        user2.setCity("上海");
        user2.setAddress("陆家嘴");

        User user3 = new User();
        user3.setId(3L);
        user3.setName("王五");
        user3.setPasswd("123456");
        user3.setCity("广州");
        user3.setAddress("天河");

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
    }

    //查找一个
    @Test
    public void findById() {
        System.out.println(userRepository.findById(1L));
    }

    //查找全部
    @Test
    public void findAll() {
        userRepository.findAll().forEach(System.out::println);
    }

    //分页
    @Test
    public void findAllPage() {
        int current = 0;
        int size = 2;
        Page<User> pages;
        do {
            Pageable pageable = PageRequest.of(current, size);
            pages = userRepository.findAll(pageable);
            log.info("当前是第 {} 页，数据：  {}", current, pages.getContent().toString());
            current++;
        } while (pages.getContent().size() > 0);


        /* Pageable page = PageRequest.of(0,2);
        Page<User> all = userRepository.findAll(page);
        System.out.println(all.getContent());
        all.getContent().forEach(System.out::println);*/
    }

    //修改
    @Test
    public void update() {
        User user = new User();
        user.setId(3L);
        user.setName("赵六");
        user.setPasswd("123456");
        user.setCity("广州");
        user.setAddress("天河");
        userRepository.save(user);
    }

    //删除
    @Test
    public void delete() {

        User user = new User();
        user.setId(3L);
        user.setName("赵六");
        user.setPasswd("123456");
        user.setCity("广州");
        user.setAddress("天河");
        userRepository.delete(user);

        userRepository.deleteById(2L);
        //userRepository.deleteById(3L);
    }


}
